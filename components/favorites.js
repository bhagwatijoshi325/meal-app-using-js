import { fetchMealsFromApi } from "../api/meals";
import { dbObjectFavList } from "../main";
import { generateOneCharString } from "../utils/helpers";
import { addRemoveToFavList, showMealDetails } from "./mealList"; // Import showMealDetails

// Show all meals added to the favorite list
export async function showFavMealList() {
  let favList = JSON.parse(localStorage.getItem(dbObjectFavList));
  let url = "https://www.themealdb.com/api/json/v1/1/lookup.php?i=";
  let html = "";

  if (favList.length == 0) {
    html = `<div class="fav-item nothing"> <h1> 
    Nothing To Show.....</h1> </div>`;
  } else {
    for (let i = 0; i < favList.length; i++) {
      const favMealList = await fetchMealsFromApi(url, favList[i]);
      if (favMealList.meals[0]) {
        let element = favMealList.meals[0];
        html += `
        <div class="fav-item" data-meal-id="${element.idMeal}" data-char-string="${generateOneCharString()}">
          <div class="fav-item-photo">
            <img src="${element.strMealThumb}" alt="">
          </div>
          <div class="fav-item-details">
            <div class="fav-item-name">
              <strong>Name: </strong>
              <span class="fav-item-text">
                ${element.strMeal}
              </span>
            </div>
            <div class="fav-like-button" data-meal-id="${element.idMeal}">
              Remove
            </div>
          </div>
        </div>`;
      }
    }
  }
  document.getElementById("fav").innerHTML = html;

  // Attach click event to all elements with class "fav-item"
  document.querySelectorAll('.fav-item').forEach(element => {
    element.addEventListener('click', function () {
      const mealId = this.getAttribute('data-meal-id');
      const charString = this.getAttribute('data-char-string');
      showMealDetails(mealId, charString);
    });
  });

  // Attach click event to all elements with class "fav-like-button"
  document.querySelectorAll('.fav-like-button').forEach(element => {
    element.addEventListener('click', function (e) {
      e.stopPropagation(); // Prevent triggering the parent's click event
      const mealId = this.getAttribute('data-meal-id');
      addRemoveToFavList(mealId);
    });
  });
}
