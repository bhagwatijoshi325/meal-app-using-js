import { fetchMealsFromApi } from "../api/meals";
import { dbObjectFavList, flexBox } from "../main";
import { truncate, isFav } from "../utils/helpers";
import { addRemoveToFavList } from "./mealList";

// Show details for a specific meal
export async function showMealDetails(itemId, searchInput) {
    console.log("searchInput:...............", searchInput);
    const list = JSON.parse(localStorage.getItem(dbObjectFavList));
    flexBox.scrollTo({ top: 0, behavior: "smooth" });
    const url = "https://www.themealdb.com/api/json/v1/1/lookup.php?i=";
    const searchUrl = "https://www.themealdb.com/api/json/v1/1/search.php?s=";
    const mealList = await fetchMealsFromApi(searchUrl, searchInput);
    console.log("mealslist:..........", mealList);
    let html = "";
    const mealDetails = await fetchMealsFromApi(url, itemId);
    if (mealDetails.meals) {
        html = `
          <div class="container remove-top-margin">
  
              <div class="header hide">
                  <div class="title">
                      Let's Eat Something New
                  </div>
              </div>
              <div class="fixed" id="search-bar">
                  <div class="icon">
                      <i class="fa-solid fa-search "></i>
                  </div>
                  <div class="new-search-input">
                      <form">
                          <input id="search-input" type="text" placeholder="Search food, receipe" />
                      </form>
                  </div>
              </div>
          </div>
          <div class="item-details">
          <div class="item-details-left">
          <img src="  ${mealDetails.meals[0].strMealThumb}" alt="">
      </div>
      <div class="item-details-right">
          <div class="item-name">
              <strong>Name: </strong>
              <span class="item-text">
              ${mealDetails.meals[0].strMeal}
              </span>
           </div>
          <div class="item-category">
              <strong>Category: </strong>
              <span class="item-text">
              ${mealDetails.meals[0].strCategory}
              </span>
          </div>
          <div class="item-ingrident">
              <strong>Ingrident: </strong>
              <span class="item-text">
              ${mealDetails.meals[0].strIngredient1},${mealDetails.meals[0].strIngredient2
            },
              ${mealDetails.meals[0].strIngredient3},${mealDetails.meals[0].strIngredient4
            }
              </span>
          </div>
          <div class="item-instruction">
              <strong>Instructions: </strong>
              <span class="item-text">
              ${mealDetails.meals[0].strInstructions}
              </span>
          </div>
          <div class="item-video">
              <strong>Video Link:</strong>
              <span class="item-text">
              <a href="${mealDetails.meals[0].strYoutube}">Watch Here</a>
            
              </span>
              <div id="like-button" data-meal-id="${mealDetails.meals[0].idMeal}">
              ${isFav(list, mealDetails.meals[0].idMeal) ? "Remove From Favourite" : "Add To Favourite"}
            </div>
          </div>
      </div>
  </div> 
          <div class="card-name">
          Related Items
      </div>
      <div id="cards-holder" class=" remove-top-margin ">`;
    }
    if (mealList.meals != null) {
        html += mealList.meals
            .map((element) => {
                return `       
              <div class="card">
              <div class="card-top" data-meal-id="${element.idMeal}" data-search-input="${searchInput}">
                      <div class="dish-photo" >
                          <img src="${element.strMealThumb}" alt="">
                      </div>
                      <div class="dish-name">
                          ${element.strMeal}
                      </div>
                      <div class="dish-details">
                          ${truncate(element.strInstructions, 50)}
                          <span class="button" onclick="showMealDetails(${element.idMeal
                    }, '${searchInput}')">Know More</span>
                      </div>
                  </div>
                  <div class="card-bottom">
                      <div class="like">
                         
                      <i class="fa-solid fa-heart ${isFav(list, element.idMeal) ? "active" : ""}" data-meal-id="${element.idMeal}"></i>
                      </div>
                      <div class="play">
                          <a href="${element.strYoutube}">
                              <i class="fa-brands fa-youtube"></i>
                          </a>
                      </div>
                  </div>
              </div>
          `;
            })
            .join("");
    }

    html = html + "</div>";

    document.getElementById("flex-box").innerHTML = html;

    // Attach click event to all elements with class "card-top"
    document.querySelectorAll('.card-top').forEach(element => {
        element.addEventListener('click', function () {
            const mealId = element.getAttribute('data-meal-id');
            const searchInputValue = element.getAttribute('data-search-input');
            showMealDetails(mealId, searchInputValue);
        });
    });

    // Attach click event to all elements with class "like"
    document.querySelectorAll('.like i').forEach(element => {
        element.addEventListener('click', function () {
            const mealId = element.getAttribute('data-meal-id');
            addRemoveToFavList(mealId);
        });
    });

    // Attach click event to the "like-button" element
    document.getElementById('like-button').addEventListener('click', function () {
        const mealId = this.getAttribute('data-meal-id');
        addRemoveToFavList(mealId);
    });
}
