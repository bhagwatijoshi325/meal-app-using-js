import { fetchMealsFromApi } from '../api/meals';
import { dbObjectFavList, updateTask } from '../main';
import { truncate, isFav } from '../utils/helpers';
import { showFavMealList } from './favorites';
import { showMealDetails } from './mealDetails';

// Show meal list based on search input
export async function showMealList() {
    const list = JSON.parse(localStorage.getItem(dbObjectFavList));
    const inputValue = document.getElementById("search-input").value;
    const url = "https://www.themealdb.com/api/json/v1/1/search.php?s=";
    const mealsData = await fetchMealsFromApi(url, inputValue);
    let html = "";
    if (mealsData.meals) {

      html = mealsData.meals
        .map((element) => {  
          return `
              <div class="card">
              <div class="card-top" data-meal-id="${element.idMeal}" data-input-value="${inputValue}">

                  <div class="dish-photo" >
                      <img src="${element.strMealThumb}" alt="">
                  </div>
                  <div class="dish-name">
                      ${element.strMeal}
                  </div>
                  <div class="dish-details">
                      ${truncate(element.strInstructions, 50)}
                      
                      <span class="button" onclick="showMealDetails(${
                        element.idMeal
                      }, '${inputValue}')">Know More</span>
                   
                  </div>
              </div>
              <div class="card-bottom">
                  <div class="like">
  
                  <i class="fa-solid fa-heart ${
                    isFav(list, element.idMeal) ? "active" : ""
                  } " onclick="addRemoveToFavList(${element.idMeal})"></i>
                  
                  </div>
                  <div class="play">
                      <a href="${element.strYoutube}">
                          <i class="fa-brands fa-youtube"></i>
                      </a>
                  </div>
              </div>
          </div>
              `;
        })
        .join("");
      document.getElementById("cards-holder").innerHTML = html;

        // Attach click event to all elements with class "card-top"
        document.querySelectorAll('.card-top').forEach(element => {
            element.addEventListener('click', function () {
                const mealId = this.getAttribute('data-meal-id');
                const inputValue = this.getAttribute('data-input-value');
                showMealDetails(mealId, inputValue);
            });
        });
    }
  }

// Add or remove a meal from the favorite list
export function addRemoveToFavList(id) {
    const detailsPageLikeBtn = document.getElementById("like-button");
    let db = JSON.parse(localStorage.getItem(dbObjectFavList));
    let ifExist = false;
    for (let i = 0; i < db.length; i++) {
      if (id == db[i]) {
        ifExist = true;
      }
    }
    if (ifExist) {
      db.splice(db.indexOf(id), 1);
    } else {
      db.push(id);
    }
  
    localStorage.setItem(dbObjectFavList, JSON.stringify(db));
    if (detailsPageLikeBtn != null) {
      detailsPageLikeBtn.innerHTML = isFav(db, id)
        ? "Remove From Favourite"
        : "Add To Favourite";
    }
  
    showMealList();
    showFavMealList();
    updateTask();
  }
