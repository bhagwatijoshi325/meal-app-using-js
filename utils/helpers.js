// Truncate string greater than 50
export function truncate(str, n) {
    return str?.length > n ? str.substr(0, n - 1) + "..." : str;
}


// Check if an ID is in a list of favorites
export function isFav(list, id) {
    let res = false;
    for (let i = 0; i < list.length; i++) {
        if (id == list[i]) {
            res = true;
        }
    }
    return res;
}

// Generates a random character string
export function generateOneCharString() {
    var possible = "abcdefghijklmnopqrstuvwxyz";
    return possible.charAt(Math.floor(Math.random() * possible.length));
}


