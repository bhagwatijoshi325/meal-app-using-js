import { fetchMealsFromApi } from './api/meals';
import { showFavMealList } from './components/favorites';
import { showMealDetails } from './components/mealDetails';
import { showMealList, addRemoveToFavList } from './components/mealList';

// Update task counter
export function updateTask() {
    const favCounter = document.getElementById("total-counter");
    const db = JSON.parse(localStorage.getItem(dbObjectFavList));
    if (favCounter.innerText != null) {
      favCounter.innerText = db.length;
    }
  }

document.addEventListener("load", () => {
  updateTask();
});

// DOM elements
export const toggleButton = document.getElementById("toggle-sidebar");
export const sidebar = document.getElementById("sidebar");
export const flexBox = document.getElementById("flex-box");
export const searchbar = document.getElementById("search-bar");

// Initialize local storage for favorite list
export const dbObjectFavList = "favouritesList";
if (localStorage.getItem(dbObjectFavList) == null) {
  localStorage.setItem(dbObjectFavList, JSON.stringify([]));
}

// Toggle sidebar and display favorite meals
toggleButton.addEventListener("click", function () {
   console.log('clicked');
  showFavMealList();
  sidebar.classList.toggle("show");
  flexBox.classList.toggle("shrink");
});

// Fix search bar on scroll
flexBox.onscroll = function () {
  if (flexBox.scrollTop > searchbar.offsetTop) {
    searchbar.classList.add("fixed");
  } else {
    searchbar.classList.remove("fixed");
  }
};



// Update task counter
updateTask();

// Show meal list based on search input
// showMealList();

// Show all meals added to the favorite list
// showFavMealList();


document.getElementById('search-form').addEventListener('keyup', showMealList);
